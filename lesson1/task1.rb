# Идеальный вес. Программа запрашивает у пользователя
# имя и рост и выводит идеальный вес по формуле
# (<рост> - 110) * 1.15, после чего выводит
# результат пользователю на экран с обращением по имени.
# Если идеальный вес получается отрицательным, то выводится
# строка "Ваш вес уже оптимальный"

puts "What's your name?"
user_name = gets.chomp

puts "Your height?"
user_height = gets.to_i

optimal_weight = (user_height - 110) / 1.15

if optimal_weight.negative?
  puts 'Ваш вес уже оптимальный :D'
else
  puts "#{user_name}, your optimal weight is #{optimal_weight.round(2)} kg."
end
