# quadratic equation
# ах^2 + bx + c = 0
# D = (b^2 – 4ac)
# if D > 0 -> 2 roots
# if D = 0 -> 1 root
# if D < 0 -> no roots

puts 'Coefficient a: '
coeff_a = gets.chomp.to_i

puts 'Coefficient b: '
coeff_b = gets.chomp.to_i

puts 'Coefficient c: '
coeff_c = gets.chomp.to_i

# discriminant
discr = coeff_b**2 - 4*coeff_a*coeff_c

# roots
# ах^2 + bx + c = 0
if discr < 0
  puts 'This equation has no roots'
elsif discr = 0
  # Х1 = Х2 = –b/(2a)
  root = -coeff_b / (2*coeff_a)
  puts "This equation has one root: #{root}"
elsif discr > 0
  # х1 = (–b + C)/(2a)
  # x2 = (–b – C)/(2a)
  root1 = (-coeff_b + Math.sqrt(discr)) / (2*coeff_a)
  root2 = (-coeff_b - Math.sqrt(discr)) / (2*coeff_a)
  puts "This equation has two roots: #{root1} and #{root2}"
end

