# Прямоугольный треугольник. Программа запрашивает у пользователя
# 3 стороны треугольника и определяет, является ли треугольник
# прямоугольным (используя теорему Пифагора www-formula.ru),
# равнобедренным (т.е. у него равны любые 2 стороны)
# или равносторонним (все 3 стороны равны) и выводит
# результат на экран. Подсказка: чтобы воспользоваться
# теоремой Пифагора, нужно сначала найти самую длинную сторону
# (гипотенуза) и сравнить ее значение в квадрате с суммой
# квадратов двух остальных сторон. Если все 3 стороны равны,
# то треугольник равнобедренный и равносторонний, но
# не прямоугольный.

sides_values = []

puts "Triangle's first side: "
first_side = gets.to_i
sides_values << first_side

puts "Triangle's second side: "
second_side = gets.to_i
sides_values << second_side

puts "Triangle's third side: "
third_side = gets.to_i
sides_values << third_side

a, b, c = sides_values.sort

if sides_values.include? 0
  abort("There's no such triangle (zero-sized side)")
elsif c >= a + b
  abort("There's no such triangle (one side is way too big)")
end

if sides_values.uniq.size == 1
  puts 'Triangle is equilateral'
end

if (sides_values.uniq.size == 3) && (c**2 == (a**2 + b**2))
  puts 'Triangle is right'
end

if (sides_values.uniq.size == 2) && (c**2 == (a**2 + b**2))
  puts 'Triangle is isosceles AND right'
end

if (sides_values.uniq.size == 2) && (a == b)
  puts 'Triangle is isosceles'
end

